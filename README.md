Temporal Closeness
==================


Given the id of a node, a time interval and a link stream dataset which is a collection of triplets 
```
id1 id2 timestamp
```
computes the temporal closeness centrality over time for the node.

 
before using
------------

- Compilation :  
```
g++ -O3 -std=c++11 temporal_closeness.cpp -o temporal_closeness
```

- Assumptions :
    + The dataset is sorted by timestamp
    + At least one link must occur at every measurement point (every time interval), 

- Adapting the format to the code :
    + if the dataset does not follow the second constraint, it can be modified by artificially adding 2 nodes that have a link at every interval.
    + interval_add.sh is a script that automatically achieves this task
    + usage:
	```
	./interval_add.sh dyn_network (gzip format) largest_existing_id interval
	```

Usage
-----

```
cat dataset | ./temporal_closeness id interval
``` 


Example
-------

- the file example.gz is a dataset of the form 
id1 id2 timestamp
sorted by timestamp, which largest node id is 1268, we measure the temporal closeness centrality of node 0 every 1000 units interval

```
./interval_add.sh example.gz 1268 1000
```
Generates a file example_interval.gz 

```
zcat example_interval.gz | ./temporal_closeness 0 1000
```




####Parameters

+ stream file: a dataset u,v,t sorted by t 
+ id: int
+ interval: int
 
Output
------ 
t c, meaning that node id as a centrality equal to c at instant t
  
Author
------
Marwan Ghanem
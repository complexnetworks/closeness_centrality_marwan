#!/bin/bash

if [ $# -ne 3 ]
then
  echo "Usage: $0 dyn_network (gzip format) largest_existing_id interval"
  exit 1
fi

nb_nodes=$2
interval=$3
filename=$(basename $1)
netw_name="${filename%.*}"

start_time=`zcat $1 | head -n 1 | awk '{print $3}'`  
end_time=`zcat $1 | tail -n 1 | awk '{print $3}'` 



start_time=$(( $start_time + $interval ))

id1=$(( $nb_nodes + 1 ))
id2=$(( $nb_nodes + 2 ))

newfile="$netw_name"_interval
touch $newfile


for i in `seq $start_time $interval $end_time`; do
   printf "%d %d %d\n" $id1 $id2 $i>> $newfile;
done

zcat $1 >> $newfile 
sort -k 3,3n $newfile | gzip -c > $newfile.gz 
rm $newfile

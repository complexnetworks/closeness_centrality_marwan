#include <iostream>
#include <sstream>
#include <fstream>
#include <set>
#include <map>
#include <vector>
#include <string>	
#include <string.h>
#include <stdio.h>      
#include <stdlib.h>
#include <assert.h>
#include <queue>


using namespace std;

int binary_search(vector<int> list,int value){
	int min,m = 0;
	int max =  list.size() - 1;

	while (true){
		if(max < min)
			return -1;
		m = (min+max) / 2;
		if(list[m] < value)
			min = m + 1;
		else if(list[m] > value)
			max = m - 1;
		else
			return m;
	}
}


void prog(int x,int interval){
	long cur_start_time = -1 ,prev_ts =-1 ,start_time = -1 ,t = -1;
	string line,stmp;
	int u,v;

	map<int,vector<pair<long,long > > > reachable_from_at = map<int,std::vector<pair<long,long > > >();
	vector<double> cum_closeness;
	vector<int> time_closeness = vector<int>();

	while (cur_start_time == -1){
		std::getline(std::cin,line);
		if(line.empty()){
			break;
		}
		istringstream iss(line);
		iss >> stmp;
		u = atoi(stmp.c_str());
		iss >> stmp;
		v = atoi(stmp.c_str());
		iss >> stmp;
		t = stol(stmp.c_str());

		if(u == x || v == x){
			cur_start_time = t;
			vector<pair<long,long > > list = vector<pair <long,long> >();
			if (u == x){
				list.push_back(make_pair(t,t));
				reachable_from_at[v] = list;
			}else{
				list.push_back(make_pair(t,t));
				reachable_from_at[u] = list;
			}
		}
		if(time_closeness.empty() || t != time_closeness.back()){
			time_closeness.push_back(t);
			cum_closeness.push_back(0.0);
		}
	}


	
	for(int i = 0 ; i < cum_closeness.size() ; i++){
		if(cur_start_time > time_closeness[i]){
			cum_closeness[i]+= 1.0 / (cur_start_time-time_closeness[i]);
		}
	}
		

	for(;std::getline(std::cin,line);){
		istringstream iss(line);
		iss >> stmp;
		u = atoi(stmp.c_str());
		iss >> stmp;
		v = atoi(stmp.c_str());
		iss >> stmp;
		t = stol(stmp.c_str());
		if(t != time_closeness.back()){
			time_closeness.push_back(t);
			cum_closeness.push_back(0.0);
		}
		if(u == x || v == x){
			cur_start_time = t;
			if (v == x){
				v = u;
				u = x;
			}	
			map<int,vector<pair<long,long > > >::iterator it;
			it = reachable_from_at.find(v);
			if(it != reachable_from_at.end()){
				vector<pair<long,long > > list = vector<pair <long,long> >();
				list = it->second;
				prev_ts = list.back().first;
				vector<pair<long,long > > tmp = vector<pair <long,long> >();
				for(int i = 0 ; i < list.size() ; i++){
					if(list[i].second < t){
						tmp.push_back(list[i]);
					}
				}
				tmp.push_back(make_pair(t,t));
				reachable_from_at[v] = tmp;
			}else{
				prev_ts = -1;
				vector<pair<long,long > > list = vector<pair <long,long> >();
				list.push_back(make_pair(t,t));
				reachable_from_at[v] = list;
			}
			/*update closeness */
			int i = time_closeness.size() - 2;
			start_time = time_closeness[i];
			if(time_closeness.size() > 3){
				while(start_time >= prev_ts){
					cum_closeness.at(i)+= 1.0 / (t - start_time);
					i = i - 1;
					if(i < 0){
						break;
					}
					start_time = time_closeness[i];
				}
			}
		/*NEITHER NODES ARE X*/
		 }else{
			map<int,vector<pair<long,long > > >::iterator it,it2;
			it = reachable_from_at.find(u);
			it2 = reachable_from_at.find(v);
			if(it != reachable_from_at.end()){
				if(it2 != reachable_from_at.end()){
					vector<pair<long,long > > lu = vector<pair <long,long> >();
					vector<pair<long,long > > list = vector<pair <long,long> >();
					vector<pair<long,long > > list2 = vector<pair <long,long> >();

					list = it->second;
					for(int i = 0 ; i < list.size() ; i++){
						if(list[i].first > it2->second.back().first && list[i].second < t){
							lu.push_back(list[i]);
						}
					}

					if(lu.size() > 0){
						int start_time_index = binary_search(time_closeness,it2->second.back().first)+1;
						long start_time_value = time_closeness[start_time_index];
						
						while(start_time_value <= lu.back().first){
							cum_closeness[start_time_index]+= 1.0/(t - start_time_value);
							start_time_index++;
							start_time_value = time_closeness[start_time_index];
						}

						vector<pair<long,long > > tmp = vector<pair <long,long> >();
						list = it2->second;
						for(int i = 0 ; i < list.size() ; i++){
							if(list[i].second  < t){
								tmp.push_back(list[i]);
							}
						}
						tmp.push_back(make_pair(lu.back().first,t));
						list = vector<pair<long,long >> (tmp.end() - min((int)tmp.size(),2),tmp.end());
						reachable_from_at[v] = list;
					
					}else{
						vector<pair<long,long > > lv = vector<pair <long,long> >();
						vector<pair<long,long > > list = vector<pair <long,long> >();
						list = it2->second;
						for(int i = 0 ; i < list.size() ; i++){
							if(list[i].first > it->second.back().first && list[i].second < t){
								lv.push_back(list[i]);
							}
						}
						if(lv.size() > 0){
							int start_time_index = binary_search(time_closeness,it->second.back().first)+1;
							long start_time_value = time_closeness[start_time_index];
							while(start_time_value <= lv.back().first){
								cum_closeness[start_time_index]+= 1.0/(t - start_time_value);
								start_time_index++;
								start_time_value = time_closeness[start_time_index];
							}	
							vector<pair<long,long > > tmp = vector<pair <long,long> >();
							list = it->second;
							for(int i = 0 ; i < list.size() ; i++){
								if(list[i].second  < t){
									tmp.push_back(list[i]);
								}
							}
							tmp.push_back(make_pair(lv.back().first,t));
							list = vector<pair<long,long >> (tmp.end() - min((int)tmp.size(),2),tmp.end());
							reachable_from_at[u] = list;
						}else{
							continue;
						}
					}
				}else{
					vector<pair<long,long > > lu = vector<pair <long,long> >();
					vector<pair<long,long > > list = vector<pair <long,long> >();
					list = it->second;
					for(int i = 0 ; i < list.size() ; i++){
						if(list[i].second < t){
							lu.push_back(list[i]);
						}
					}
					if(lu.size() > 0){
						vector<pair<long,long > > tmp = vector<pair <long,long> >();
						tmp.push_back(make_pair(lu.back().first,t));
						reachable_from_at[v] = tmp;
						int start_time_index = 0;
						long start_time_value = time_closeness[start_time_index];
						while(start_time_value <= lu.back().first){
							cum_closeness[start_time_index]+= 1.0/(t - start_time_value);
							start_time_index++;
							start_time_value = time_closeness[start_time_index];
						}
					}
				}
			}else if (it2 != reachable_from_at.end()){
						vector<pair<long,long > > lv = vector<pair <long,long> >();
						vector<pair<long,long > > list = vector<pair <long,long> >();
						list = it2->second;
						for(int i = 0 ; i < list.size() ; i++){
							if(list[i].second < t){
								lv.push_back(list[i]);
							}
						}
						if(lv.size() > 0){
							vector<pair<long,long > > tmp = vector<pair <long,long> >();
							tmp.push_back(make_pair(lv.back().first,t));
							reachable_from_at[u] = tmp;
							int start_time_index = 0;
							long start_time_value = time_closeness[start_time_index];
							while(start_time_value <= lv.back().first){
								cum_closeness[start_time_index]+= 1.0/(t - start_time_value);
								start_time_index++;
								start_time_value = time_closeness[start_time_index];
							}
						}
					}

				}
	}


	for(int i = 0 ; i < cum_closeness.size() ; i++){
		if((time_closeness[i] - time_closeness[0]) % interval == 0)
			cout << time_closeness[i] << " " << cum_closeness[i] << " " << x <<endl;
	}
}


int main(int argc,char * argv[]){
	int n,interval ;
	assert( sscanf(argv[1], "%d", &n) == 1);
	assert( sscanf(argv[2], "%d", &interval) == 1);
	prog(n,interval);
}
